package learn.collections;
import java.util.*;
//samouczek programisty
//Napisz program, który będzie pobierał od użytkownika imiona.
// Program powinien pozwolić użytkownikowi na wprowadzenie dowolnej liczby imion
// (wprowadzenie „-” jako imienia przerwie wprowadzanie).
// Na zakończenie wypisz liczbę unikalnych imion.
public class Names {

    public static final String NO_MORE_NAMES = "-";
    public static void main(String[] args) {
        String name = "";
        Set<String> names = new HashSet<>();
        Scanner input = new Scanner(System.in);
        while (!name.equals(NO_MORE_NAMES)) {
            System.out.println("Wpisz imię: ");
            name = input.nextLine();
            if(!name.equals(NO_MORE_NAMES)){
                names.add(name);
            }
        }
        System.out.println("Liczba unikalnych imion: " + names.size());
    }
}
