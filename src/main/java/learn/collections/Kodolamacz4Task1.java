package learn.collections;

/*Dla tablicy wejściowej input zawierającą liczby typu int
zwrócić tablicę zawierającą ilość elementów ujemnych oraz sumę elementów dodatnich.
Jeśli tablica będzie pusta lub null, wtedy należy zwrócić pustą tablicę.*/

import java.util.HashSet;
import java.util.Set;

public class Kodolamacz4Task1 {
    public static void main(String[] args) {
        int[] input = {1,2,3,4,5,-1,-2,-3};
        int[] emptyOne = {};
        countAndSumElements(input);
        countAndSumElements(emptyOne);

    }
    public static int[] countAndSumElements(int[] input) {
        if(input == null || input.length == 0) {
            return new int[0];
        }

        int counter = 0;
        int sum = 0;
        for (int liczba:input) {
            if(liczba<0) {
                counter++;
            } else {
                sum += liczba;
            }
        }
        System.out.println("counter: " + counter + " sum: " + sum);

        return new int[]{counter,sum};

    }
}

