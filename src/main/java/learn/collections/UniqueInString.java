package learn.collections;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class UniqueInString {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Type a word: ");
        String toCheck = sc.nextLine();

        Set<Character> characters = new HashSet<Character>() ;
        for (char c : toCheck.toCharArray()) {
            characters.add(c);
        }

        if(toCheck.length() == characters.size()) {
            System.out.println("All characters are unique.");
        } else {
            System.out.println("Characters are not unique.");
        }
    }

}
