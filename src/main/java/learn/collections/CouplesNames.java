package learn.collections;

import java.util.*;

//samouczek programisty
//Napisz program, który będzie pobierał od użytkownika imiona par dopóki nie wprowadzi imienia „-”,
// następnie poproś użytkownika o podanie jednego z wcześniej wprowadzonych imion i wyświetl imię odpowiadającego mu partnera.
public class CouplesNames {
    public static void main(String[] args) {
        String firstName, secondName;
        Scanner input = new Scanner(System.in);
        Map<String,String> couplesNames = new HashMap<>();

        while(true) {
            System.out.println("Podaj pierwsze imię: ");
            firstName = input.nextLine();
            if(firstName.equals("-")){
                break;
            }

            System.out.println("Podaj drugie imię: ");
            secondName = input.nextLine();
            if(secondName.equals("-")){
                break;
            }
            couplesNames.put(firstName, secondName);
        }

        //iteracja po mapie
        System.out.println("Iterowanie po kluczach i wartosciach");
        for(Map.Entry<String, String> entry : couplesNames.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + ": " + value);
        }

        System.out.println("Czyją połówkę sprawdzić?");
        firstName = input.nextLine();
        System.out.println("Parę z " + firstName + " tworzy " + couplesNames.get(firstName));
    }
}
