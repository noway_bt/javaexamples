package learn.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Lotto {
    public static void main(String[] args) {
        /*int[] showResults;
        showResults = draw(5, 50);
        System.out.println(Arrays.toString(showResults));*/
        System.out.println(Arrays.toString(draw(50,50)));
    }

    public static int[] draw(int k, int n) {
        Random crazy = new Random();
        List<Integer> allNumbers = new ArrayList<>(n);
        int[] results = new int[k];

        for (int i = 1; i <= n; i++) {
            allNumbers.add(i);
        }
        for(int j =0; j<k ;j++) {
            int index = crazy.nextInt(allNumbers.size());
            results[j] = allNumbers.get(index);
            allNumbers.remove(index);
        }

        return results;
    }
}
