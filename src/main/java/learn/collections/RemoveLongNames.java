package learn.collections;

//from JetBrains

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;

class RemoveLongNames {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, String> map = new HashMap<>();

        while (map.size() < 3) {
            String s = scanner.nextLine();
            String[] pair = s.split(" ");
            map.put(pair[0], pair[1]);
        }

        removeLongNames(map);
        System.out.println(map.size());
    }


    public static void removeLongNames(Map<String, String> countriesMap) {
        // write your code here
//        for (Map.Entry<String, String> mapEntry : countriesMap.entrySet() ) {
//            String key = mapEntry.getKey();
//            String value = mapEntry.getValue();
//            if(key.length() > 7 || value.length() > 7) {
//                countriesMap.remove(key);
//            }
//        }

        //stream:
//        countriesMap.entrySet().stream()
//                .filter(m -> m.getKey().length() > 7 || m.getValue().length() > 7)
//                .collect()

//        Map<String, String> copy = new HashMap<>(countriesMap);
//        copy.entrySet()
//                .stream()
//                .filter(m -> m.getKey().length() > 7 || m.getValue().length() > 7)
//                .forEach(item -> countriesMap.remove(item.getKey()));


        Map<String, String> copy = new HashMap<>(countriesMap);
        copy.entrySet()
                .stream()
                .filter(m -> m.getKey().length() > 7 || m.getValue().length() > 7)
                .map(Map.Entry::getKey)
                .forEach(countriesMap::remove);

    }
}




