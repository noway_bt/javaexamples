package learn.collections;

import java.util.Comparator;
import java.util.List;

public class MathUtils {
    public static double max(List<Double> numbersList) {
       return numbersList.stream().max(Double::compareTo).get();
    }

    public static double min(List<Double> numbersList) {
        return numbersList.stream().min(Double::compareTo).get();
    }
}
