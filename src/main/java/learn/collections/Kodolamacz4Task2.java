package learn.collections;

import java.util.*;

/* Należy stworzyć algorytm sterujący pracą automatu z napojami.
      Zakładamy, że automat przyjmuje monety 1zł, 2zł i 5zł.
      Puszka napoju kosztuje 1zł.
      Na starcie automat nie ma żadnych pieniędzy.
      Dla wskazanej listy wejściowej monet, należy zwrócić true lub false
      w zależności czy automat będzie w stanie zwrócić resztę.
      Każda moneta jest wrzucana przez innego klienta.*/
public class Kodolamacz4Task2 {
        public static void main(String[] args) {
                List<Integer> coinsOne = Arrays.asList(1,2,1,1,5);
                List<Integer> coinsTwo = Arrays.asList(1,2,2,5,2);
                List<Integer> coinsThree = Arrays.asList(1,2,1,5,2);

                /*List<Integer> coins = new ArrayList<>();
                coins.add(1);
                coins.add(2);
                coins.add(1);
                coins.add(1);
                coins.add(5);*/
                //List coins = List.of(1,2,1,1,5);

                System.out.println("for: ");
                for (int i = 0; i < coinsOne.size(); i++) {
                        System.out.println("Element nr: " + i + " zawiera wartość: " + coinsOne.get(i) );
                }

                System.out.println("foreach: ");
                for (Integer e: coinsOne) {
                        System.out.println("Element nr: " + coinsOne.indexOf(e) + " zawiera wartość: " + e );
                }
                System.out.println("reszta dla coinsOne: ");
                vendingMachine(coinsOne);

                System.out.println("reszta dla coinsTwo: ");
                vendingMachine(coinsTwo);

                System.out.println("reszta dla coinsThree: ");
                vendingMachine(coinsThree);


        }

        public static boolean vendingMachine(List<Integer> coins) {

                int count1PLN = 0;
                int count2PLN = 0;
                int count5PLN = 0;

                for (Integer e: coins) {
                        if(e == 1) {
                                count1PLN++;

                        } else if(e == 2) {
                                if(count1PLN >= 1) {
                                    count2PLN++;
                                    count1PLN--;
                                } else {
                                        System.out.println("nie ma 1PLN żeby wydać z 2PLN");
                                        return false;
                                }
                        } else if(e == 5) {
                                if(count2PLN >= 2) {
                                      count5PLN++;
                                      count2PLN-=2;
                                } else if(count2PLN == 1 && count1PLN >= 2) {
                                        count5PLN++;
                                        count2PLN--;
                                        count1PLN-=2;
                                } else if(count1PLN >= 4) {
                                        count5PLN++;
                                        count1PLN-=4;
                                } else {
                                        System.out.println("brak monet, żeby wydać resztę z 5PLN");
                                        return false;
                                }
                        } else {
                                System.out.println("automat przyjmuje tylko: 1, 2 i 5 PLN");
                                return false;
                        }
                }
                System.out.println("wydano resztę ");
                return true;
        }
}
