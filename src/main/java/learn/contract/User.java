package learn.contract;

import java.util.Objects;

// https://1024kb.pl/nauka-java/equals-hash-code-co-moze-pojsc-nie-tak/
//User class without equals and hashCode method
public class User {
    final int id;
    final String login;
    int age;

    public User(int id, String login, int age ) {
        this.id = id;
        this.login = login;
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
