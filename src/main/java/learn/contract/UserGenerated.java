package learn.contract;
import java.util.Objects;

//User class with generated equals and hashCode method
public class UserGenerated extends User{

    public UserGenerated(int id, String login, int age) {
        super(id, login, age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserGenerated user = (UserGenerated) o;
        return id == user.id && age == user.age && Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, age);
    }

}
