package learn.contract;

import java.util.Objects;

//User class with implemented equals and hashCode method
public class UserOwnImpl extends User {
    public UserOwnImpl (int id, String login, int age) {
        super(id, login, age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserOwnImpl user = (UserOwnImpl) o;
        return id == user.id && login.equals(user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login);
    }


}
