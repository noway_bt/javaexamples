package learn.contract;

public class ContractEqualsHash {
    public static void main(String[] args) {
        Integer integerOne = new Integer(12345678);
        Integer integerTwo = new Integer(12345678);
        System.out.println (integerOne == integerTwo) ; //false
        System.out.println (integerOne.equals(integerTwo)) ; //true

        System.out.println("integerOne = " + integerOne.hashCode()); //12345678
        System.out.println("integerTwo = " + integerTwo.hashCode()); //12345678

        String s1 = "Aa";
        String s2 = "BB";
        String s3 = "BB";
        String s4 = new String("BB");

        System.out.println(s1.equals(s2)); // false
        System.out.println(s1.hashCode()); //2112
        System.out.println(s2.hashCode()); //2112

        System.out.println(s2.equals(s3));//true
        System.out.println(s2 == s3);//true

        System.out.println(s2.equals(s4));//true
        System.out.println(s2 == s4);//false
    }
}
