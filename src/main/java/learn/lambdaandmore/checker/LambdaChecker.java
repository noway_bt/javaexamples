package learn.lambdaandmore.checker;

public class LambdaChecker {
    public static void main(String[] args) {
        Checker<Integer> isOddLambda = object -> object % 2 != 0;

        System.out.println(isOddLambda.check(123));
        System.out.println(isOddLambda.check(124));
    }
}
