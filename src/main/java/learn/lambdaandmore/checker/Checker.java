package learn.lambdaandmore.checker;

public interface Checker <T>{
    boolean check(T object);
}
