package learn.lambdaandmore.greeting;

public interface GreetingModule {
    void sayHello();
}
