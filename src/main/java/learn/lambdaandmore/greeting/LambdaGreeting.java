package learn.lambdaandmore.greeting;

public class LambdaGreeting {
    public static void main(String[] args) {
            AnonymousGreeting.Robot jan = new AnonymousGreeting.Robot(() -> System.out.println("dzień dobry"));

            AnonymousGreeting.Robot john = new AnonymousGreeting.Robot(() -> System.out.println("good morning"));
    }
}
