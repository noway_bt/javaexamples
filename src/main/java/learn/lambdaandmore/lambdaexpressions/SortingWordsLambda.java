package learn.lambdaandmore.lambdaexpressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortingWordsLambda {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Waldemar", "Wojtek", "Ania", "Jan");
        System.out.println("Before sorting: " + words);
        //lambda:
        Collections.sort(words, (w1, w2) -> w1.length()-w2.length());
        System.out.println("After sorting: " + words);

        /*
        Collections.sort(words,new Comparator<String>(){
            @Override
            public int compare(String w1, String w2) {
                return w1.length()-w2.length();
            }
        });
        */
    }
}
