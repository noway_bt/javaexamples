package learn.lambdaandmore.lambdaexpressions;
// https://www.samouczekprogramisty.pl/klasy-wewnetrzne-i-anonimowe-w-jezyku-java/
import java.util.*;

public class SortingWords {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> wordsList = new LinkedList<>();

        for (int i = 0; i < 5; i++) {
            System.out.println("Type a word: ");
            wordsList.add(scanner.nextLine());
        }

        System.out.println("Before sorting: " + wordsList);

        Collections.sort(wordsList, Comparator.comparingInt(String::length));

        System.out.println("After sorting: " + wordsList);
    }
}
