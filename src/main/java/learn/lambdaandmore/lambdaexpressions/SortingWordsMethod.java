package learn.lambdaandmore.lambdaexpressions;

import java.util.*;

public class SortingWordsMethod {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> wordsList = new LinkedList<>();

        for (int i = 0; i < 5; i++) {
            System.out.println("Type a word: ");
            wordsList.add(scanner.nextLine());
        }

        System.out.println("Before sorting: " + wordsList);

        //method reference:
        Collections.sort(wordsList, SortingWordsMethod::compareByLength);

        System.out.println("After sorting: " + wordsList);
    }

    public static int compareByLength(String w1, String w2) {
        return w1.length() - w2.length();
    }

}
