package learn.lambdaandmore.lambdaexpressions;

import java.util.Arrays;
import java.util.List;

public class MethodReference {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4);
        /*Consumer<Integer> integerConsumer = n -> System.out.println(n);
        numbers.forEach(integerConsumer);*/
        numbers.forEach(System.out::println);
    }
}
