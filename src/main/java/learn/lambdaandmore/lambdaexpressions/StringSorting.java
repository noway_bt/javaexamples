package learn.lambdaandmore.lambdaexpressions;

import java.util.*;

//samouczek lambda
public class StringSorting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> stringsList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            System.out.println("Type a word: ");
            stringsList.add(scanner.nextLine());
        }

        System.out.println("Before sorting: " + stringsList);
        Collections.sort(stringsList, (s1, s2) -> s2.length() - s1.length());
        System.out.println("After sorting: " + stringsList);
    }
}
