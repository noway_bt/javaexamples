package learn.abstractclass.figure;

 abstract class Figure {

    abstract double Area();
    abstract double Perimeter(); //obwód
}
