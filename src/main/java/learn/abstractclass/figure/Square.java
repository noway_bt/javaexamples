package learn.abstractclass.figure;

public class Square extends Figure{
    public double a;

    public Square(double a) {
        this.a = a;
    }

    public double Area()
    {
        return a*a;
    }

    public double Perimeter()
    {
        return 4*a;
    }
}
