package learn.abstractclass.figure;

public class FigureTest {
    public static void main(String[] args) {
        Figure square2 = new Square(10);
        Square square1 = new Square(5);

        System.out.println("Area of square1: " + square1.Area());
        System.out.println("Perimeter of square1: " + square1.Perimeter());

        Rectangle rectangle1 = new Rectangle(5, 2);

        System.out.println("Area of rectangle1: = " + rectangle1.Area());
        System.out.println("Perimeter of rectangle1 = " + rectangle1.Perimeter());
    }
}
