package learn.abstractclass.figure;

public class Rectangle extends Figure{

    public double a, b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double Area()
    {
        return a*b;
    }

    public double Perimeter()
    {
        return 2*a + 2*b;
    }
}
