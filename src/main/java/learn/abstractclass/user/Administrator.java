package learn.abstractclass.user;

public class Administrator extends User{
    @Override
    public String type() {
        return "ADMINISTRATOR_USER";
    }
}
