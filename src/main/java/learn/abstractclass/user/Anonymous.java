package learn.abstractclass.user;

public class Anonymous extends User{
    @Override
    public String type() {
        return "ANONYMOUS_USER";
    }
}
