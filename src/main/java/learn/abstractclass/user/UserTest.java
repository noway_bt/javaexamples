package learn.abstractclass.user;

public class UserTest {
    public static void main(String[] args) {
        Administrator adm = new Administrator();
        Anonymous anonim = new Anonymous();

        System.out.println(adm.someText());
        System.out.println(adm.type());
        System.out.println(anonim.type());
    }
}
