package learn.abstractclass.user;

public abstract class User {
    private int theNumber = 7;

    public abstract String type();

    public String someText() {
        return "SOME_TEXT " + theNumber;
    }
}
