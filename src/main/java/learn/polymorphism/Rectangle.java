package learn.polymorphism;

public class Rectangle implements Figure{
    private double a, b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double calculateSurface()
    {
        return a*b;
    }

    public double calculatePerimeter()
    {
        return 2*a + 2*b;
    }
}
