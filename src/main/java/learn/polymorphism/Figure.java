package learn.polymorphism;

public interface Figure {
    double calculateSurface();
    double calculatePerimeter();
}
