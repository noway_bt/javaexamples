package learn.polymorphism;

public class Triangle implements Figure{

    private double a, b, c, h;

    public Triangle(double a, double b, double c, double h) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.h = h;
    }

    @Override
    public double calculateSurface() {
        return a*h/2;
    }

    @Override
    public double calculatePerimeter() {
        return a+b+c;
    }
}
