package learn.polymorphism;

public class Circle implements Figure{
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double calculateSurface() {
        return Math.PI * Math.pow(r,2);
    }

    @Override
    public double calculatePerimeter() {
        return 2 * Math.PI * r;
    }
}
