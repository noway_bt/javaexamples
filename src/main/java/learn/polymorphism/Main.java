package learn.polymorphism;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Figure circle = new Circle(2);
        Circle circle = new Circle(2);
        Rectangle rectangle = new Rectangle(3,5);
        Triangle triangle = new Triangle(1,2,3,4);

        List<Figure> figureList = new ArrayList<>();
        figureList.add(circle);
        figureList.add(rectangle);
        figureList.add(triangle);

        figureList.forEach(figure -> System.out.println("Surface " + figure.getClass().getSimpleName() + ": " + figure.calculateSurface()));
        System.out.println(" ");
        figureList.forEach(figure -> System.out.println("Perimeter " + figure.getClass().getSimpleName() + ": " + figure.calculatePerimeter()));

    }
}
