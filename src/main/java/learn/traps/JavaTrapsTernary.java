package learn.traps;

import java.util.Random;

//https://javastart.pl/baza-wiedzy/efektywne-programowanie/javatraps-002
public class JavaTrapsTernary {
    public static void main(String[] args) {
        Random random = new Random();
        boolean check = random.nextBoolean();

        Number number = (check || !check) ? new Integer(1) : new Double(2.0);

        System.out.println("Result: " + number);

    }
}
