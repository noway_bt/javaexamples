package learn.algorithms.quicksort;
// http://www.algorytm.org/algorytmy-sortowania/sortowanie-szybkie-quicksort/quick-j.html

import java.io.Console;
import java.util.Scanner;

public class QuickSort {

    private static void sortQuick(int numbers[]) {
        sortQuick(numbers, 0, numbers.length - 1);
    }

    private static void sortQuick(int numbers[], int x, int y) {
        int i = x;
        int j = y;
        int pivot = numbers[(x + y) / 2];
        int temp;

        do {
            while (numbers[i] < pivot)
                i++;
            while (pivot < numbers[j])
                j--;
            if (i <= j) {
                temp = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = temp;
                i++;
                j--;
            }
        }
        while (i <= j);
        if (x < j) {
            sortQuick(numbers, x, j);
        }
        if (i < y)
            sortQuick(numbers, i, y);
    }

    public static void main(String[] args) {

        System.out.println("How many numbers to sort?");
        Scanner sc = new Scanner(System.in);
        int howManyNumbers = sc.nextInt();
        int numbersArray[] = new int[howManyNumbers];

        for (int i = 0; i < howManyNumbers; i++) {
            System.out.println("Enter a number #" + (i + 1));
            numbersArray[i] = sc.nextInt();
        }

        System.out.println("Array before sorting: ");
        for (int i = 0; i < howManyNumbers; i++) {
            System.out.println(numbersArray[i]);
        }

        sortQuick(numbersArray);

        System.out.println("Array after sorting: ");
        for (int i = 0; i < howManyNumbers; i++) {
            System.out.println(numbersArray[i]);
        }
    }
}
