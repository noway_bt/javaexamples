package learn.algorithms.quicksort;
//https://strefakodera.pl/algorytmy/algorytmy-sortowania/przerabiamy-algorytm-quicksort-z-wersji-rekurencyjnej-na-nierekurencyjna

public class Qsort {
    static int arr[] = {45, -17, 71, 21, 3};
    static void qSort(int left, int right) {
        if( right <= left ) {
            return;
        }
        else {
            int q = partition(left, right);
            qSort(left, q - 1);
            qSort(q + 1, right);
        }
    }

    static int partition(int left, int right) {
        long x = arr[left];
        int i = left;

        for( int j = left + 1; j <= right; j++ ) {
            if( arr[j] <= x ) {
                i++;
                swap(i, j);
            }
        }
        swap(i, left);
        return i;
    }
    static void swap(int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        qSort(0,4);
        for (int i = 0; i < arr.length ; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
