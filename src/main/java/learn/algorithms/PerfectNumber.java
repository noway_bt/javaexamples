package learn.algorithms;
//Mateusz Rus, Mailowa Akademia Programowania

public class PerfectNumber {
    public static boolean checkNumber(int number) {
        if(number <= 0) {
            throw new IllegalArgumentException("Number must be greater than 0");
        }
        int sum = 0;
            for (int i = 1; i <= number/2; i++) {
                if (number % i == 0) {
                    sum += i;
                }
            }
            if(sum == number) {
                return true;
            }
            else {
                return false;
            }
    }
    public static void main(String[] args) {
        System.out.println("True, perfect: ");
        System.out.println(checkNumber(6));
        System.out.println(checkNumber(28));
        System.out.println(checkNumber(496));
        System.out.println(checkNumber(8128));
        System.out.println("Not perfect: ");
        System.out.println(checkNumber(5));
        System.out.println(checkNumber(546));
        System.out.println(checkNumber(75));
        System.out.println(checkNumber(0));
        System.out.println(checkNumber(-6));
    }
}
