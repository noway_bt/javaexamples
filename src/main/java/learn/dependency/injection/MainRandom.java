package learn.dependency.injection;

import java.util.Random;

public class MainRandom {
    public static void main(String[] args) {
        RandomString randomString = new RandomString();
        System.out.println(randomString.getRandomString(3));

        RandomStringDI randomStringDI = new RandomStringDI(new Random());
        System.out.println("DI: " + randomStringDI.getString(10));
    }
}
