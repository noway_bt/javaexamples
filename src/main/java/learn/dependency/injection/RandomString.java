package learn.dependency.injection;

//samouczek programisty: https://www.samouczekprogramisty.pl/dependency-inversion-principle-dependency-injection-i-inversion-of-control/
import java.util.Random;
import java.util.stream.Collectors;

public class RandomString {
    private final Random generator = new Random();

    public String getRandomString(int length) {
        return generator.ints(length, 'a', 'z' + 1)
                .mapToObj(i -> String.valueOf((char) i))
                .collect(Collectors.joining());
    }
}
