package learn.dependency.injection;

import java.util.Random;

public class RandomStringDI {
    private final Random generator;

    public RandomStringDI(Random generator) {
        this.generator = generator;
    }

    public String getString(int length) {
        return generator.ints(length, 'a', 'z' + 1)
                .mapToObj(i -> (char) i)
                .reduce(new StringBuilder(), StringBuilder::append, StringBuilder::append)
                .toString();
    }
}
