package learn.exceptions;

//javastart.pl
public class StackOverflowErrorExample {
    public static void main(String[] args) {
        int sum = sum(1000000);
        System.out.println(sum);
    }

    static int sum(int limit) {
        return limit >= 1 ? limit + sum(limit - 1) : limit;
    }
}
