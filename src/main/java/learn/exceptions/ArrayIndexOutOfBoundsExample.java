package learn.exceptions;

import java.util.Scanner;

public class ArrayIndexOutOfBoundsExample {
    public static void main(String[] args) {
        int[] arrayOne = {10, 2, 30, 40, 50};
        Scanner scanner = new Scanner(System.in);

        System.out.println("Which array element to display?");
        int index = scanner.nextInt();
        scanner.close();
        try {
            System.out.println(arrayOne[index]);
        } catch(ArrayIndexOutOfBoundsException e) {
            System.err.println("\n" +
                    "You entered an index that exceeds the size of the array!");
        }
    }
}
