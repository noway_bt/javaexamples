package learn.exceptions;

import java.util.Scanner;

//javastart.pl
public class NumberFormatExceptionExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give me some number: ");
        String text = scanner.nextLine();
        try {
            int number = Integer.parseInt(text);
            System.out.println("Your number is: " + number);
        } catch(NumberFormatException e) {
            System.err.println("This is not a number!");
        }
        finally {
            scanner.close();
        }
    }
}
