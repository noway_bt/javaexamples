package learn.exceptions.nullpointerexample;

public class NullPointerTest2 {
    public static void main(String[] args) {
        Person[] persons = new Person[10]; //by default an array is filled with null values
        persons[0].setName("Henry");
    }
}
