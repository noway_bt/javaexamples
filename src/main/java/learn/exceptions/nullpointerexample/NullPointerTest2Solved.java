package learn.exceptions.nullpointerexample;

public class NullPointerTest2Solved {
    public static void main(String[] args) {
        Person[] persons = new Person[10];
        for(int i=0; i<persons.length; i++) {
            persons[i] = new Person(); // assign objects to the array
        }
        persons[0].setName("Henry");
        System.out.println(persons[0].getName());
    }
}
