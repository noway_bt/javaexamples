package learn.exceptions.nullpointerexample;

public class NullPointerTest {
    public static void main(String[] args) {
        Person person = null;
        person.getName(); //calling the method on null value
    }
}
