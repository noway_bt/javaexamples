package learn.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.lang.Math;

//samouczekprogramisty.pl
public class RootExercise {

    public static void main(String[] args) {

        System.out.println("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        double numberFromUser = 0;
        while (true) {
            try {
                numberFromUser = scanner.nextDouble();
                break;
            } catch (InputMismatchException e) {
                System.out.println("Wrong format.");
                scanner.next();
            }
        }

        if (numberFromUser < 0) {
            throw new IllegalArgumentException(String.format("You entered negative number.", numberFromUser));
        }

        System.out.println("The square root of the given number is: " + Math.sqrt(numberFromUser));
    }
}
