package learn.exceptions;

public class StackOverflowSolved {
    public static void main(String[] args) {
        int sum = sum(1000000);
        System.out.println(sum);
    }

    static int sum(int limit) {
        int sum = 0;
        for(int i = 1; i <= limit; i++) {
            sum += i;
        }
        return sum;
    }
}
