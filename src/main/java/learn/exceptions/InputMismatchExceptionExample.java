package learn.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputMismatchExceptionExample {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Give me some number:");
        try {
            int number = sc.nextInt();
            System.out.println("Your number is: " + number);
        } catch(InputMismatchException e) {
            System.err.println("This is not a number!");
        }
        finally {
            sc.close();
        }
    }
}
