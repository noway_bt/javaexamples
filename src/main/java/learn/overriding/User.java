package learn.overriding;

public class User {
    private String login;
    private String firstName;
    private String lastName;

    @Override
    public String toString() {
        return
                "login=" + login +
                ",firstName=" + firstName +
                ",lastName=" + lastName;
    }
}
