package learn.patterns.factory.headfirst.factorymethod;

public class NYStyleCheesePizza extends PizzaFM {
    public NYStyleCheesePizza() {
        name = "NY Style Sauce and Cheese Pizza";
        dough = "Thin Crust Dough";
        sauce = "Marinara Sauce";

        toppings.add("Grated Reggiano Cheese");
    }
}
