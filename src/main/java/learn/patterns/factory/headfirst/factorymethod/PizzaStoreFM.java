package learn.patterns.factory.headfirst.factorymethod;

public abstract class PizzaStoreFM {
    abstract PizzaFM createPizza(String item);

    public PizzaFM orderPizza(String type) {
        PizzaFM pizza = createPizza(type);
        System.out.println("--- Making a " + pizza.getName() + " ---");
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
