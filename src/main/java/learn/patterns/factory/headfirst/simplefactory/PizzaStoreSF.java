package learn.patterns.factory.headfirst.simplefactory;

public class PizzaStoreSF {
    SimplePizzaFactory factory;

    public PizzaStoreSF(SimplePizzaFactory factory) {
        this.factory = factory;
    }

    public PizzaSF orderPizza(String type) {
        PizzaSF pizza;

        pizza = factory.createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }
}
