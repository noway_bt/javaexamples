package learn.patterns.factory.headfirst.simplefactory;

public class ClamPizza extends PizzaSF {
    public ClamPizza() {
        name = "Clam Pizza";
        dough = "Thin crust";
        sauce = "White garlic sauce";
        toppings.add("Clams");
        toppings.add("Grated parmesan cheese");
    }
}
