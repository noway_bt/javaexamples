package learn.patterns.factory.headfirst.simplefactory;

public class PizzaTestSF {
    public static void main(String[] args) {
        SimplePizzaFactory factory = new SimplePizzaFactory();
        PizzaStoreSF store = new PizzaStoreSF(factory);

        PizzaSF pizza = store.orderPizza("cheese");
        System.out.println("We ordered a " + pizza.getName() + "\n");

        pizza = store.orderPizza("veggie");
        System.out.println("We ordered a " + pizza.getName() + "\n");
    }
}
