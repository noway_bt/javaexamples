package learn.patterns.factory.refguru.fmethod;

public class ITDepartment extends Department{
    @Override
    public Employee createEmployee(String name) {
        return new Programmer(name, 10000);
    }
}
