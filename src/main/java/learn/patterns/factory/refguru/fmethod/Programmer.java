package learn.patterns.factory.refguru.fmethod;

public class Programmer extends Employee{
    public Programmer(String name, int salary) {
        super(name, salary);
    }
}
