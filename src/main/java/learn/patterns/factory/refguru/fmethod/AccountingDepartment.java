package learn.patterns.factory.refguru.fmethod;

public class AccountingDepartment extends Department{
    @Override
    public Employee createEmployee(String name) {
        return new Accountant(name, 5000);
    }
}
