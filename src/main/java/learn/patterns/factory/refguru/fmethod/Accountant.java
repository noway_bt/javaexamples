package learn.patterns.factory.refguru.fmethod;

public class Accountant extends Employee{
    public Accountant(String name, int salary) {
        super(name, salary);
    }
}
