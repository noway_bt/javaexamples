package learn.patterns.factory.refguru.fmethod;

public class CompanyDemo {
    public static void main(String[] args) {
        Department departmentAcc = new AccountingDepartment();
        Employee employee1 = departmentAcc.createEmployee("Lukasz");
        employee1.paySalary();
        employee1.dismiss();

        Department departmentIT = new ITDepartment();
        Employee employee2 = departmentIT.createEmployee("Ania");
        employee2.paySalary();
        employee2.dismiss();

        departmentIT.fire("Tomek");
        departmentAcc.fire("Hania");

    }
}
