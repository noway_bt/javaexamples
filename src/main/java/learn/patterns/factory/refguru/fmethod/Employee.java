package learn.patterns.factory.refguru.fmethod;

public class Employee {
    private String name;
    private int salary;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }
    public void paySalary() {
        System.out.println("Pay salary " + salary);
    };

    public void dismiss() {
        System.out.println("Bye " + name);
    }
}
