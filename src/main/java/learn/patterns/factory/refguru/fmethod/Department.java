package learn.patterns.factory.refguru.fmethod;

abstract class Department {
    public abstract Employee createEmployee(String name);

    public void fire(String name) {
        Employee employee = createEmployee(name);
        employee.paySalary();
        employee.dismiss();
    }
}
