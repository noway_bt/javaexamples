package learn.patterns.factory.refguru.fabstract.app;

import learn.patterns.factory.refguru.fabstract.factories.GUIFactory;
import learn.patterns.factory.refguru.fabstract.factories.MacOSFactory;
import learn.patterns.factory.refguru.fabstract.factories.WindowsFactory;

/**
 * Application picks the factory type and creates it in run time (usually at
 * initialization stage), depending on the configuration or environment
 * variables.
 */
public class Demo {

    public static void main(String[] args) {
        Application app = configureApplication();
        app.paint();
    }

    private static Application configureApplication() {
        Application app;
        GUIFactory factory;
        String osName = System.getProperty("os.name").toLowerCase();
        if(osName.contains("mac")) {
            factory = new MacOSFactory();
            app = new Application(factory);
        } else if(osName.contains("windows")) {
            factory = new WindowsFactory();
            app = new Application(factory);
        } else {
            throw new UnsupportedOperationException("Unsupported OS");
        }
        return app;
    }
}
