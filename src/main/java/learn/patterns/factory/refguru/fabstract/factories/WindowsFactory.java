package learn.patterns.factory.refguru.fabstract.factories;

import learn.patterns.factory.refguru.fabstract.buttons.Button;
import learn.patterns.factory.refguru.fabstract.buttons.WindowsButton;
import learn.patterns.factory.refguru.fabstract.checkboxes.Checkbox;
import learn.patterns.factory.refguru.fabstract.checkboxes.WindowsCheckbox;

public class WindowsFactory implements GUIFactory{

    @Override
    public Button createButton() {
        return new WindowsButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new WindowsCheckbox();
    }
}
