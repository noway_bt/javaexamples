package learn.patterns.factory.refguru.fabstract.factories;

import learn.patterns.factory.refguru.fabstract.buttons.Button;
import learn.patterns.factory.refguru.fabstract.buttons.MacOSButton;
import learn.patterns.factory.refguru.fabstract.checkboxes.Checkbox;
import learn.patterns.factory.refguru.fabstract.checkboxes.MacOSCheckbox;

public class MacOSFactory implements GUIFactory {

    @Override
    public Button createButton() {
        return new MacOSButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new MacOSCheckbox();
    }
}
