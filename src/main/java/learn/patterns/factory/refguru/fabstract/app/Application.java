package learn.patterns.factory.refguru.fabstract.app;

import learn.patterns.factory.refguru.fabstract.buttons.Button;
import learn.patterns.factory.refguru.fabstract.checkboxes.Checkbox;
import learn.patterns.factory.refguru.fabstract.factories.GUIFactory;

public class Application {
    private Button button;
    private Checkbox checkbox;

    public Application(GUIFactory factory) {
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }

    public void paint() {
        button.paint();
        checkbox.paint();
    }
}
