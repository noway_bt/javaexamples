package learn.patterns.factory.refguru.fabstract.factories;

import learn.patterns.factory.refguru.fabstract.buttons.Button;
import learn.patterns.factory.refguru.fabstract.checkboxes.Checkbox;

public interface GUIFactory {
    Button createButton();
    Checkbox createCheckbox();
}
