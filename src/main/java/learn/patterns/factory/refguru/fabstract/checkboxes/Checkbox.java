package learn.patterns.factory.refguru.fabstract.checkboxes;

public interface Checkbox {
    void paint();
}
