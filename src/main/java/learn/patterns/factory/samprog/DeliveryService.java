package learn.patterns.factory.samprog;
// https://www.samouczekprogramisty.pl/wzorzec-projektowy-metoda-wytworcza/
public interface DeliveryService {
    void deliver(Parcel parcel);
}
