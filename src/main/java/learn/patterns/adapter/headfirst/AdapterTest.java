package learn.patterns.adapter.headfirst;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

public class AdapterTest {

    public static void main(String[] args) {
        ArrayList<String> fruitsList = new ArrayList<>();
        fruitsList.add("Mango");
        fruitsList.add("Banana");
        fruitsList.add("Strawberry");
        fruitsList.add("Apple");

        //Traversing list through Iterator
        Iterator itr = fruitsList.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }

        //Traversing list through Enumeration (IteratorAdapter)
        Iterator itr2 = fruitsList.iterator();
        Enumeration enumeration = new IteratorAdapter(itr2);
        while(enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement());
        }

    }
}
