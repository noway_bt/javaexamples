package learn.patterns.decorator.cafe;

public class WhippedCream extends IngredientDecorator{
    public WhippedCream(Drink drink) {
        super(drink);
    }
    @Override
    public String getDescription() {
        return drink.getDescription() + ", Whipped cream";
    }
    @Override
    public double cost() {
        return drink.cost() + 0.30;
    }
}
