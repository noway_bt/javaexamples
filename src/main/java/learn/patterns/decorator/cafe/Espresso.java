package learn.patterns.decorator.cafe;

public class Espresso extends Drink{
    public Espresso() {
        description = "espresso coffee";
    }

    public double cost() {
        return 1.99;
    }
}
