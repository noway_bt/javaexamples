package learn.patterns.decorator.cafe;

public class StarCafeSpecial extends Drink{
    public StarCafeSpecial() {
        description = "Coffee Star Cafe Special";
    }
    public double cost() {
        return 0.89;
    }
}
