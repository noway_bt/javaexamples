package learn.patterns.decorator.cafe;

public abstract class Drink {
    String description = "unknown drink";

    public String getDescription() {
        return description;
    }

    public abstract double cost();

    //public abstract CupSize getSize();
}
