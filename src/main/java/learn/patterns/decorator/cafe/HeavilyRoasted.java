package learn.patterns.decorator.cafe;

public class HeavilyRoasted extends Drink{
    public HeavilyRoasted() {
        description = "heavily roasted coffee";
    }

    public double cost() {
        return 0.99;
    }
}
