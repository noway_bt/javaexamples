package learn.patterns.decorator.cafe;

public class Chocolate extends IngredientDecorator{

    public Chocolate(Drink drink) {
        super(drink);
    }
    public String getDescription() {
        return drink.getDescription() + ", Chocolate";
    }
    public double cost() {
        return drink.cost() + 0.20;
    }
}
