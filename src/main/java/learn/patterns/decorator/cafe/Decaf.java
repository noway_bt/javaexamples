package learn.patterns.decorator.cafe;

public class Decaf extends Drink{
    public Decaf() {
        description = "decaf coffee";
    }

    public double cost() {
        return 1.05;
    }
}
