package learn.patterns.decorator.cafe;

public class SoyMilk extends IngredientDecorator {

    public SoyMilk(Drink drink) {
        super(drink);
    }

    @Override
    public String getDescription() {
        return drink.getDescription() + ", soy milk";
    }

    @Override
    public double cost() {
        return drink.cost() + 0.10;
    }
}
