package learn.patterns.decorator.cafe;

public enum CupSize {
    SMALL, MEDIUM, LARGE;
}
