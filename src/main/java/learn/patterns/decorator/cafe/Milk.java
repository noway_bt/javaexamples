package learn.patterns.decorator.cafe;

public class Milk extends IngredientDecorator{
    public Milk(Drink drink) {
        super(drink);
    }
    @Override
    public String getDescription() {
        return drink.getDescription() + ", Milk";
    }

    @Override
    public double cost() {
        return drink.cost() + 0.10;
    }


}
