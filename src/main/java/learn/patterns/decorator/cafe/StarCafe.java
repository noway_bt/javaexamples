package learn.patterns.decorator.cafe;
//headfirst, design patterns
public class StarCafe {
    public static void main(String[] args) {
        Drink drink1 = new Espresso();
        System.out.println(drink1.getDescription() + " " + drink1.cost() + " PLN");

        Drink drink2 = new HeavilyRoasted();
        drink2 = new Chocolate(drink2);
        drink2 = new Chocolate(drink2);
        drink2 = new WhippedCream(drink2);
        System.out.println(drink2.getDescription() + " " + drink2.cost() + " PLN");

        Drink drink3 = new StarCafeSpecial();
        drink3 = new SoyMilk(drink3);
        drink3 = new Chocolate(drink3);
        drink3 = new WhippedCream(drink3);
        System.out.println(drink3.getDescription() + " " + drink3.cost() + " PLN");
    }
}
