package learn.patterns.decorator.cafe;

public abstract class IngredientDecorator extends Drink{
    protected Drink drink;

    public IngredientDecorator(Drink drink) {
        this.drink = drink;
    }

    public abstract String getDescription();
}
