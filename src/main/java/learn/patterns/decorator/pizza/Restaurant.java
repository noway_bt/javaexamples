package learn.patterns.decorator.pizza;
//blog: samouczek programisty
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

public class Restaurant {
    public static void main(String[] args) {
        Pizza margherita = new Pizza();
        Pizza withMozzarella = new PizzaWithMozzarella(margherita);
        Pizza withMozzarellaAndHam = new PizzaWithHam(withMozzarella);
        Pizza withMozzarellaHamAndBasil = new PizzaWithBasil(withMozzarellaAndHam);
        Pizza withMozzarellaAndBasil = new PizzaWithBasil(withMozzarella);

        DecimalFormat df = new DecimalFormat("#,00 zł");
        for (Pizza pizza : List.of(margherita, withMozzarella, withMozzarellaAndHam, withMozzarellaHamAndBasil, withMozzarellaAndBasil)) {
            System.out.println(String.format("%s costs %s.", pizza, df.format(pizza.getPrice())));
        }

        Pizza withSth = new PizzaWithTopping(withMozzarella, new BigDecimal(5));
        System.out.println("pizza mix kosztuje: " + withSth.getPrice());
        withSth.toString();
    }
}
