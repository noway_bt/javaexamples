package learn.patterns.observer.article;

public class ArticleObserverMain {
    public static void main(String[] args) {
        Blog blogLearnJava = new Blog();
        Reader readerJohn = new Reader("John");
        Reader readerAna = new Reader("Ana");

        blogLearnJava.attach(readerJohn);
        blogLearnJava.attach(readerAna);


        ArticleEvent newArticle = new ArticleEvent("pattern observable", "https://www.samouczekprogramisty.pl/wzorzec-projektowy-obserwator/");
        blogLearnJava.notifyObservers(newArticle);
    }
}
