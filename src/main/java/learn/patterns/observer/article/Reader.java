package learn.patterns.observer.article;

public class Reader implements Observer{
    private String readerName;

    public Reader(String readerName) {
        this.readerName = readerName;
    }

    public void update(ArticleEvent newestArticle) {
        System.out.println(String.format("Dear %s, an article „%s” was published!",readerName, newestArticle));
    }
}
