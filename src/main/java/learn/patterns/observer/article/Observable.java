package learn.patterns.observer.article;

public interface Observable {
    void attach(Observer observer);
    void detach(Observer observer);
    void notifyObservers(ArticleEvent articleEvent);
}
