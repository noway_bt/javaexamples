package learn.patterns.observer.article;

public class ArticleEvent {
   private String title;
   private String link;

    public ArticleEvent(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "ArticleEvent{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
