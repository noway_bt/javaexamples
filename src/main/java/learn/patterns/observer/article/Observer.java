package learn.patterns.observer.article;

public interface Observer {
    void update(ArticleEvent newestArticle);
}
