package learn.patterns.observer.article;

import java.util.HashSet;
import java.util.Set;

public class Blog implements Observable{
    private Set<Observer> observers = new HashSet<>();

    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(ArticleEvent newestArticle) {
        observers.forEach(observer -> observer.update(newestArticle));
    }
  }
