package learn.patterns.observer.weatherstation;

public class CurrentConditionsDisplay implements Observer, DisplayItem{
    private float temp;
    private float humidity;
    private Subject WeatherData;

    public CurrentConditionsDisplay(Subject WeatherData) {
        this.WeatherData = WeatherData;
        WeatherData.registerObserver(this);
    }

    public void update(float temp, float humidity, float pressure) {
        this.temp = temp;
        this.humidity = humidity;
        display();
    }

    public void display() {
        System.out.println("Current conditions " + temp + " degrees and " + humidity + "% humidity.");
    }
}
