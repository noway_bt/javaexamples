package learn.patterns.observer.weatherstation;

public interface DisplayItem {
    public void display();
}
