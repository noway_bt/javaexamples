package learn.staticword;

// Używa metody static.
class StaticMeth {
    static int val = 1024; // zmienna static

    // metoda static
    static int valDiv2() {
        return val / 2;
    }
}

public class SDemo2 {
    public static void main(String args[]) {

        System.out.println("Wartość składowej val:" + StaticMeth.val);
        System.out.println("StaticMeth.valDiv2(): " + StaticMeth.valDiv2());
        StaticMeth.val = 4;
        System.out.println("Wartość składowej val:" + StaticMeth.val);
        System.out.println("StaticMeth.valDiv2(): " + StaticMeth.valDiv2());
    }
}

