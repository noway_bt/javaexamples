package learn.staticword;

// Używa bloku static.
class StaticBlock {
    static double rootOf2;
    static double rootOf3;
    static { // Ten blok zostaje wykonany podczas ładowania klasy.
        System.out.println("Wewnątrz bloku static.");
        rootOf2 = Math.sqrt(2.0);
        rootOf3 = Math.sqrt(3.0);
    }
    StaticBlock(String msg) {
        System.out.println(msg);
    }
}
public class SDemo3 {
    public static void main(String args[]) {
        StaticBlock ob = new StaticBlock("Wewnątrz konstruktora.");
        System.out.println("Pierwiastek kwadratowy z 2 wynosi " + StaticBlock.rootOf2);
        System.out.println("Pierwiastek kwadratowy z 3 wynosi " + StaticBlock.rootOf3);
    }
}
