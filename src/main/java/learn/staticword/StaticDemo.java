package learn.staticword;
//https://strefainzyniera.pl/artykul/940/slowo-kluczowe-static

// Używa zmiennej static.
public class StaticDemo {
    int x; // zwykła zmienna składowa
    static int y; // zmienna static Istnieje tylko jedna kopia składowej y, wspólna dla wszystkich obiektów.

    int sum() {
        return x + y;
    }

    static int square() {
        return y * y;
    }

}
class SDemo {
    public static void main(String args[]) {
        StaticDemo ob1 = new StaticDemo();
        StaticDemo ob2 = new StaticDemo();
        // Każdy obiekt ma własną kopię składowej x.
        ob1.x = 10;
        ob2.x = 20;

        System.out.println("Oczywiście, składowe ob1.x i ob2.x " + "są niezależne.");
        System.out.println("ob1.x: " + ob1.x + " ob2.x: " + ob2.x);
        System.out.println();
        // Każdy obiekt współdzieli z innymi jedną kopię zmiennej static.
        System.out.println("Zmienna y jest zadeklarowana jako static i tym samym współdzielona.");
        StaticDemo.y = 19;
        System.out.println("Nadaje StaticDemo.y wartość 19.");
        System.out.println("ob1.sum(): " + ob1.sum());
        System.out.println("ob2.sum(): " + ob2.sum());
        System.out.println();
        StaticDemo.y = 100;
        System.out.println("Zmienia wartość StaticDemo.y na 100");
        System.out.println("ob1.sum(): " + ob1.sum());
        System.out.println("ob2.sum(): " + ob2.sum());
        System.out.println();


        System.out.println("Static method: ");
        System.out.println(StaticDemo.square());
    }
}
