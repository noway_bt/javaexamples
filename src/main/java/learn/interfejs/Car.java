package learn.interfejs;

public interface Car {
    int CONSTANT = 17; //public static final by default

    default String name() {
        return "I'm a car!";
    }

    default int sayANumber() {
        return sayConstant();
    }

    static String sayHello() {
        return "HELLO!";
    }

    static String sayAnotherHello() {
        return sayLoudHello();
    }

    private static String sayLoudHello() {
        return "LOUD HELLO!";
    }

    private int sayConstant() {
        return CONSTANT;
    }
}
