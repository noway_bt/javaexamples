package learn.stream.boardgame;

import java.util.Comparator;
import java.util.stream.Stream;

public class LoopToStream {
    public static void main(String[] args) {
        loopVersion();
        streamVersion();
        streamVersionOptional();
    }

    private static void loopVersion() {
        double highestRanking = 0;
        BoardGame bestGame = null;
        for (BoardGame game : BoardGame.GAMES) {
            if (game.name.contains("a")) {
                if (game.rating > highestRanking) {
                    highestRanking = game.rating;
                    bestGame = game;
                }
            }
        }
        System.out.println(bestGame.name);
    }

    private static void streamVersion() {
        BoardGame theBestGame = BoardGame.GAMES
                .stream()
                .filter(game -> game.name.contains("a"))
                .max(Comparator.comparingDouble(g1 -> g1.rating)).get();
        System.out.println(theBestGame.name);
    }

    private static void streamVersionOptional() {
        BoardGame.GAMES.stream()
                .filter(g -> g.name.contains("a"))
                .max(Comparator.comparingDouble(g1 -> g1.rating))
                .map(item -> item.name)
                .ifPresent(System.out::println);
    }
}
