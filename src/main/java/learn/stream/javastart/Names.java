package learn.stream.javastart;

import java.util.Arrays;
import java.util.List;

public class Names {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Dorota", "Paweł", "Wojtek", "Karolina");
        System.out.println(names);

        names.stream()
                .filter(s -> s.endsWith("a"))
                .forEach(System.out::println);

    }
}
