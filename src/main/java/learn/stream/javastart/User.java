package learn.stream.javastart;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class User {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Dorota","Zosia", "Paweł", "Wojtek","Ania", "Karolina");
        List<User> userList = names.stream()
                .filter(n -> n.endsWith("a"))
                .map(string -> new User(string, new Random().nextInt(70)))
                .sorted(Comparator.comparing(User::getName).reversed())
                .collect(Collectors.toList());

        System.out.println(userList);
    }

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
