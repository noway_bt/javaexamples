package learn.stream.example;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamFindAndFilterExample {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        List<Person> fullAged = Persons.getPersons().stream()
                .filter(person -> Years.yearsBetween(person.getDateOfBirth(), now).getYears() >= 18)
                .collect(Collectors.toList());

        System.out.println("FullAged(" + fullAged.size() + "):" + fullAged.stream().map(Object::toString).collect(Collectors.joining(", ")));

        System.out.println("First person is: " + Persons.getPersons().stream().findFirst());

        Optional<Person> personWithName = Persons.getPersons().stream()
                .filter(person -> person.getName().equalsIgnoreCase("Piotr"))
                .findFirst();
        System.out.println("Found person with name Piotr: " + personWithName);
    }
}
