package learn.stream.example;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.util.function.Consumer;
import java.util.function.Function;

public class StreamMapPrintExamples {
    public static void main(String[] args) {

        Persons.getPersons().stream()
                .peek(StreamMapPrintExamples::printPersonAge)
                .map(Object::toString)
                .forEach(System.out::println);


        System.out.println("_____________###################________________");

        //the same
        Persons.getPersons().stream()
                .peek(person -> StreamMapPrintExamples.printPersonAge(person))
                .map(person -> person.toString())
                .forEach(string -> System.out.println(string));

        System.out.println("_____________###################________________");

        //the same
        Persons.getPersons().stream()
                .peek(new Consumer<Person>() {
                    @Override
                    public void accept(Person person) {
                        StreamMapPrintExamples.printPersonAge(person);
                    }
                })
                .map(new Function<Person, String>() {
                    @Override
                    public String apply(Person person) {
                        return person.toString();
                    }
                })
                .forEach(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        System.out.println(s);
                    }
                });

        System.out.println("_____________###################________________");
    }

    public static void printPersonAge(Person person) {
        System.out.println(person.getName() + " has " + Years.yearsBetween(person.getDateOfBirth(), LocalDate.now()).getYears() + " years");
    }
}
