package learn.stream.example;

import org.joda.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Persons {
    public static List<Person> getPersons(){
        return Arrays.asList(
                new Person("John","Nowak", LocalDate.parse("1978-01-18")),
                new Person("James","Bond", LocalDate.parse("1987-07-14")),
                new Person("Jane","Doe", LocalDate.parse("2013-03-29")),
                new Person("Harry","Potter", LocalDate.parse("2001-07-30")),
                new Person("Gandalf","Szary", LocalDate.parse("-5000-01-18")));
    }
}
