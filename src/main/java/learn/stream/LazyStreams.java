package learn.stream;

import java.util.stream.IntStream;

public class LazyStreams {
    public static void main(String[] args) {
        IntStream numbersStream = IntStream.range(0, 8);
        System.out.println("Before");
        numbersStream = numbersStream.filter(n -> n % 2 == 0);
        System.out.println("during 1");
        numbersStream = numbersStream.map(n -> {
            System.out.println("> " + n);
            return n;
        });
        System.out.println("during 2");
        numbersStream = numbersStream.limit(2);
        System.out.println("during 3");
        numbersStream.forEach(System.out::println);
        System.out.println("After");
    }
}
