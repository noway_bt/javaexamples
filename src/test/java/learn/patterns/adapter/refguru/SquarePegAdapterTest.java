package learn.patterns.adapter.refguru;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SquarePegAdapterTest  {
    @Test
    public void shouldCalculateRadius() {
        //given
        SquarePegAdapter squarePegAdapter = new SquarePegAdapter(new SquarePeg(2));
        //when
        double result = squarePegAdapter.getRadius();
        //then
        assertEquals(1.4142135623730951, result,0.000001);
    }
}