package learn.dependency.injection;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class RandomStringTest {
    @Test
    void shouldReturnExpectedLength() {
        //given
        int stringLength = 10;
        RandomString randomString = new RandomString();

        //when
        String result = randomString.getRandomString(stringLength);

        //then
        assertEquals(stringLength, result.length());
    }

   /* @Test
    void shouldReturnExpectedString() {
        //given
        int stringLength = 10;
        RandomString randomString = new RandomString();

        //when
        String result = randomString.getRandomString(stringLength);

        //then
        assertEquals("", result); //cannot test random without DI, can't control Random object
    }*/

}