package learn.dependency.injection;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Random;

import static learn.collections.MathUtils.max;
import static org.junit.jupiter.api.Assertions.*;

class RandomStringDITest {
    @Test
    void shouldReturnExpectedLength() {
        //given
        int stringLength = 10;
        RandomStringDI randomStringDI = new RandomStringDI(new Random());

        //when
        String result = randomStringDI.getString(stringLength);

        //then
        assertEquals(stringLength, result.length());
    }

    @Test
    void shouldReturnExpectedString() {
        //given
        int stringLength = 10;
        RandomStringDI randomStringDI = new RandomStringDI(new Random(18L));

        //when
        String result = randomStringDI.getString(stringLength);

        //then
        assertEquals("aeiotgswwm", result);
    }

    @Test
    void shouldReturnEmptyStringWhenZero() {
        //given
        int stringLength = 0;
        RandomStringDI randomStringDI = new RandomStringDI(new Random());

        //when
        String result = randomStringDI.getString(stringLength);

        //then
        assertEquals("", result);
    }

    @Test
    void shouldThrowIllegalArgumentExWhenMinus() {
        //given
        int stringLength = -3;
        RandomStringDI randomStringDI = new RandomStringDI(new Random());

        //when & then
        assertThrows(IllegalArgumentException.class, () -> {
            randomStringDI.getString(stringLength);
        });
    }
}