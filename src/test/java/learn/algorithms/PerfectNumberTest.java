package learn.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PerfectNumberTest {
    @Test
    void shouldReturnTrueWhenNumberIsPerfect() {
        //given
        int checkThatNumber = 6;

        //when
        boolean result = PerfectNumber.checkNumber(checkThatNumber);

        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseWhenNotPerfectNumber() {
        //given
        int checkThatNumber = 7;

        //when
        boolean result = PerfectNumber.checkNumber(checkThatNumber);

        //then
        assertFalse(result);
    }

    @Test
    void shouldThrowExceptionWhenNegativeNumber() {
        //given
        int checkThatNumber = -6;
        //when & then
        assertThrows(IllegalArgumentException.class, () -> {
            PerfectNumber.checkNumber(checkThatNumber);
        });
    }

    @Test
    void shouldThrowExceptionWhenZero() {
        //given
        int checkThatNumber = 0;

        //when & then
        assertThrows(IllegalArgumentException.class, () -> {
            PerfectNumber.checkNumber(checkThatNumber);
        });
    }

    @Test
    void shouldNotFailWhenMaxInt() {
        //given
        int checkThatNumber = Integer.MAX_VALUE;
        //when
        boolean result = PerfectNumber.checkNumber(checkThatNumber);
        //then
        assertFalse(result);
    }
}