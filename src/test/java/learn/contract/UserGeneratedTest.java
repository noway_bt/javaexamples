package learn.contract;

import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;

import java.util.*;

class UserGeneratedTest implements WithAssertions {
    private User user;

    @Test
    void shouldReturnListFromMap() {
        //given
        User user = new UserGenerated(1, "Pablo", 10);
        Map<User, List<String>> map = new HashMap<>();
        map.put(user, Collections.singletonList("Value1"));

        //when
        List<String> values = map.get(user);

        //then
        assertThat(values)
                .hasSize(1)
                .contains("Value1");
    }
    @Test
    void shouldReturnUserFromMapWhenGetValueUsingCopyOfKey() {
        //given
        User user = new UserGenerated(1, "Pablo", 10);
        User pablo = new UserGenerated(1, "Pablo", 10);
        Map<User, List<String>> map = new HashMap<>();
        map.put(user, Collections.singletonList("Value1"));

        //when
        List<String> values = map.get(pablo);

        //then
        assertThat(values)
                .hasSize(1)
                .contains("Value1");
    }
    @Test
    void shouldReturnUserFromMapEvenWhenUserStateHasChanged() {
        //given
        User user = new UserGenerated(1, "Pablo", 10);
        Map<User, List<String>> map = new HashMap<>();
        map.put(user, Collections.singletonList("Value1"));

        //when
        //user.setAge(15);
        List<String> values = map.get(user);

        //then
        assertThat(values)
                .hasSize(1)
                .contains("Value1");
    }
}