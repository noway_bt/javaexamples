package learn.contract;

import org.junit.jupiter.api.Test;
import java.util.*;
import static org.assertj.core.api.Assertions.assertThat;

class UserTest {
    @Test
    void shouldReturnStringFromMap() {
        //given
        User user = new User(1, "Pablo", 10);
        Map<User, String> map = new HashMap<>();
        map.put(user, "Value1");

        //when
        String stringValue = map.get(user);

        //then
        assertThat(stringValue).isEqualTo("Value1");
    }

    @Test
    void shouldReturnListFromMap() {
        //given
        User user = new User(1, "Pablo", 10);
        Map<User, List<String>> map = new HashMap<>();
        map.put(user, Collections.singletonList("Value1"));

        //when
        List<String> values = map.get(user);

        //then
        assertThat(values)
                .hasSize(1)
                .contains("Value1");
    }

    @Test
    void shouldReturnUserFromMapWhenGetValueUsingCopyOfKey() {
        //given
        User user = new User(1, "Pablo", 10);
        User pablo = new User(1, "Pablo", 10);
        Map<User, List<String>> map = new HashMap<>();
        map.put(user, Collections.singletonList("Value1"));

        //when
        List<String> values = map.get(pablo);

        //then
        assertThat(values)
                .isNull();
//                .hasSize(1)
//                .contains("Value1");
        //Without equals and hashCode implementation java uses default one,
        // objects user and pablo are not equals here.
        // Conclusion - to compare objects based on fields, we need to implement the equals and hashCode methods,
        // otherwise the objects are compared based on references.
    }

}