package learn.collections;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static learn.collections.MathUtils.max;
import static learn.collections.MathUtils.min;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MathUtilsTest {
    @Test
    void shouldFindMaxValue() {
        //given
        List<Double> testList = Arrays.asList(20.3,19.6,77.8,158.9,23.0);

        //when
        Double result = max(testList);

        //then
        assertThat(result)
                .isEqualTo(158.9);
    }

    @Test
    void shouldFindMinValue() {
        //given
        List<Double> testList = Arrays.asList(20.3,19.6,77.8,158.9,23.0);

        //when
        Double result = min(testList);

        //then
        assertThat(result)
                .isEqualTo(19.6);
    }

    @Test
    void shouldThrowNoSuchElementExceptionWhenEmptyList() {
        //given
        List<Double> testList = Collections.emptyList();

        //when & then
        Exception exception = assertThrows(NoSuchElementException.class, () -> {
            max(testList);
        });
    }

    @Test
    void shouldFindWhenAllTheSame() {
        //given
        List<Double> testList = Arrays.asList(19.6,19.6,19.6);

        //when
        Double result = min(testList);

        //then
        assertThat(result)
                .isEqualTo(19.6);
    }

}